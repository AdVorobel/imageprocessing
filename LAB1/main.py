import matplotlib.pyplot as plt
import numpy as np
from matplotlib import image

# Images
HIGH_CONTRAST = image.imread('img/high_contrast.jpeg')
HIGH_DETAILED = image.imread('img/high_detailed.webp')
LOW_CONTRAST = image.imread('img/low_contrast.jpg')
LOW_DETAILED = image.imread('img/low_detailed.jpeg')

IMAGES = ((HIGH_CONTRAST, 'high_contrast'),
          (HIGH_DETAILED, 'high_detailed'),
          (LOW_CONTRAST, 'low_contrast'),
          (LOW_DETAILED, 'low_detailed'))


def histogram_shift(img: np.array, k: int) -> np.array:
    return np.maximum(0, img + k) if k < 0 else np.minimum(255, img + k)


def compare_images(originals, transformed, k):
    if len(originals) != len(transformed):
        raise AttributeError('Arrays should be same length')

    fig, axes = plt.subplots(len(originals), 2)
    fig.tight_layout()
    fig.set_size_inches(10, 10)
    for i in range(len(originals)):
        original, label, transform = *originals[i], transformed[i]
        axes[i][0].imshow(original)
        axes[i][0].set_title(label, fontsize=12)
        axes[i][1].imshow(transform)
        axes[i][1].set_title(f'k={k}', fontsize=12)

    plt.show()


if __name__ == '__main__':
    k = -50
    transformed = [histogram_shift(img, k) for img, label in IMAGES]
    compare_images(IMAGES, transformed, k)
