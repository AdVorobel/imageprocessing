import copy

import matplotlib.pyplot as plt
import numpy as np
import cv2 as cv
from matplotlib import image

# Images
HIGH_CONTRAST = image.imread('img/high_contrast.jpg')
HIGH_CONTRAST_MOD = image.imread('img/high_contrast_m.jpg')
LOW_CONTRAST = image.imread('img/low_contrast.jpeg')
LOW_CONTRAST_MOD = image.imread('img/low_contrast_m.jpeg')
BRIGHT_INTENSITY = image.imread('img/bright_intensity.jpeg')
BRIGHT_INTENSITY_MOD = image.imread('img/bright_intensity_m.jpeg')
DARK_INTENSITY = image.imread('img/dark_intensity.jpeg')
DARK_INTENSITY_MOD = image.imread('img/dark_intensity_m.jpeg')

IMGS = {
    "high_contrast": (HIGH_CONTRAST, HIGH_CONTRAST_MOD),
    "low_contrast": (LOW_CONTRAST, LOW_CONTRAST_MOD),
    "bright_intensity": (BRIGHT_INTENSITY, BRIGHT_INTENSITY_MOD),
    "dark_intensity": (DARK_INTENSITY, DARK_INTENSITY_MOD),
}


def show_matches(img1, img2, kp1, kp2, matches):
    img3 = cv.drawMatches(img1, kp1, img2, kp2, matches, None, flags=cv.DrawMatchesFlags_NOT_DRAW_SINGLE_POINTS)
    plt.figure(figsize=(18, 10))
    plt.imshow(img3, cmap=plt.get_cmap('Greys'))
    plt.show()


def euclidean(v1, v2):
    return np.sqrt(sum((np.array(v1) - np.array(v2)) ** 2))


def fast_detector(img1, img2):
    fast = cv.FastFeatureDetector_create()
    return fast.detect(img1, None), fast.detect(img2, None)


def brief_descriptor(img1, img2, kp1, kp2):
    brief = cv.xfeatures2d.BriefDescriptorExtractor_create()
    kp1, des1 = brief.compute(img1, kp1)
    kp2, des2 = brief.compute(img2, kp2)
    return des1, kp1, des2, kp2


def bf_matcher(img1: np.array, img2: np.array):
    kp1, kp2 = fast_detector(img1, img2)
    des1, kp1, des2, kp2 = brief_descriptor(img1, img2, kp1, kp2)
    best_matches = []
    for idx1, point1 in enumerate(des1):
        distances = [(idx, euclidean(point1, point2)) for idx, point2 in enumerate(des2)]
        idx2, min_dist = min(distances, key=lambda x: x[1])
        best_matches.append(cv.DMatch(_imgIdx=0, _queryIdx=idx1, _trainIdx=idx2, _distance=min_dist))

    return best_matches, kp1, kp2


def knn_matcher(img1: np.array, img2: np.array, k: int = 2, ratio: float = 0.75):
    kp1, kp2 = fast_detector(img1, img2)
    des1, kp1, des2, kp2 = brief_descriptor(img1, img2, kp1, kp2)
    best_matches = []
    for idx1, point1 in enumerate(des1):
        distances = [(idx, euclidean(point1, point2)) for idx, point2 in enumerate(des2)]
        k_matches = sorted(distances, key=lambda x: x[1])[:k]
        closest1, closest2 = k_matches[:2]
        if closest1[1] < ratio * closest2[1]:
            idx2, min_dist = closest1
            best_matches.append(cv.DMatch(_imgIdx=0, _queryIdx=idx1, _trainIdx=idx2, _distance=min_dist))

    return best_matches, kp1, kp2


if __name__ == '__main__':
    img1, img2 = IMGS['high_contrast']
    # img1, img2 = IMGS['dark_intensity']
    img1_gray, img2_gray = cv.cvtColor(img1, cv.COLOR_BGR2GRAY), cv.cvtColor(img2, cv.COLOR_BGR2GRAY)

    matches, kp1, kp2 = bf_matcher(img1_gray, img2_gray)
    show_matches(img1, img2, kp1, kp2, matches[::10])

    matches, kp1, kp2 = knn_matcher(img1_gray, img2_gray, ratio=0.5)
    show_matches(img1, img2, kp1, kp2, matches[::5])
